﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Forms;

namespace PvTerrenos
{
    public class User : CRUD
    {
        public int id_user { get; set; }
        public string nombre { get; set; }
        public string password { get; set; }
        public NivelAcceso tipo { get; set; }
        public string administracion { get; set; }
        public string fecha_creacion { get; set; }
        public int fk_empleado { get; set; }

        private string table = "";

        public List<User> _list = new List<User>();

        public User()
        {
            table = USUARIO;
        }

        #region METODOS
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }

        public int idUser(string _nombre)
        {
            return _list.Find(x => x.nombre == _nombre).id_user;
        }

        public string nombreUsuario(int id_usuario)
        {
            try
            {
                return _list.Find(x => x.id_user == id_usuario).nombre;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public void login()
        {
            if (tipo == NivelAcceso.ADMINISTRADOR | tipo == NivelAcceso.ADMINISTRADORMM | tipo == NivelAcceso.SISTEMAS | tipo == NivelAcceso.SUPERVISOR)
            {
                FrmVentaLote venta_lote = new FrmVentaLote(this);
                venta_lote.Show();
            }
            if (tipo == NivelAcceso.CAJERO)
            {
                FrmPuntoVenta punto_venta = new FrmPuntoVenta(this);
                punto_venta.Show();
            }
            if (tipo == NivelAcceso.COBRANZA)
            {
                FrmConsultaMorosos consultaMorosos = new FrmConsultaMorosos();
                consultaMorosos.Show();
            }
        }

        public void loadList()
        {
            _list = list<User>();
        }

        public void insertLastId()
        {
            int index = _list.Count() - 2;

            int penultimoId = _list[index].id_user;

            int ultimoId = penultimoId + 1;

            _list.Last().id_user = ultimoId;
        }
        #endregion

        #region METODOS INTERFACE
        public void loadComboBox(ComboBox cb, bool vendedores)
        {
            //query("1", $"{WHERE} ?");
            /*cb.DataSource = user_list.Select(user => user.nombre).ToList();
            cb.SelectedIndex = -1;*/

            cb.Items.Clear();

            if (vendedores)
            {
                foreach (var user in _list.FindAll(x => x.tipo == NivelAcceso.VENDEDOR))
                {
                    cb.Items.Add(user.nombre.ToUpper());
                }
            }
            else
            {
                foreach (var user in _list)
                {
                    cb.Items.Add(user.nombre.ToUpper());
                }
            }
        }
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"{nombre.ToLower().Trim()}{cc}" +
            $"{password.ToLower().Trim()}{cc}" +
            $"{tipo}{cc}" +
            $"{administracion.ToUpper().Trim()}{cc}" +
            $"{fecha_creacion.ToUpper().Trim()}{cc}" +
            $"{fk_empleado}";
        }
        #endregion
    }
}
