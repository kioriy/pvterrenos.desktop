﻿namespace PvTerrenos
{
    partial class FrmConsultaMorosos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpFechaMorosos = new System.Windows.Forms.DateTimePicker();
            this.cmdRealizarConsulta = new System.Windows.Forms.Button();
            this.dgvMorosos = new System.Windows.Forms.DataGridView();
            this.lRegistros = new System.Windows.Forms.Label();
            this.rowNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFechaCompra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cUltimoPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFechaUltimoPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPredio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cManzana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cLote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowTelefono1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowTelefono2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowFechaCita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowHora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowNumeroCita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMorosos)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpFechaMorosos
            // 
            this.dtpFechaMorosos.Location = new System.Drawing.Point(12, 43);
            this.dtpFechaMorosos.Name = "dtpFechaMorosos";
            this.dtpFechaMorosos.Size = new System.Drawing.Size(214, 20);
            this.dtpFechaMorosos.TabIndex = 0;
            // 
            // cmdRealizarConsulta
            // 
            this.cmdRealizarConsulta.Location = new System.Drawing.Point(232, 41);
            this.cmdRealizarConsulta.Name = "cmdRealizarConsulta";
            this.cmdRealizarConsulta.Size = new System.Drawing.Size(75, 23);
            this.cmdRealizarConsulta.TabIndex = 1;
            this.cmdRealizarConsulta.Text = "Buscar";
            this.cmdRealizarConsulta.UseVisualStyleBackColor = true;
            this.cmdRealizarConsulta.Click += new System.EventHandler(this.cmdRealizarConsulta_Click);
            // 
            // dgvMorosos
            // 
            this.dgvMorosos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMorosos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMorosos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rowNombre,
            this.cFechaCompra,
            this.cUltimoPago,
            this.cFechaUltimoPago,
            this.cPredio,
            this.cManzana,
            this.cLote,
            this.rowTelefono1,
            this.rowTelefono2,
            this.rowFechaCita,
            this.rowHora,
            this.rowNumeroCita});
            this.dgvMorosos.Location = new System.Drawing.Point(12, 69);
            this.dgvMorosos.Name = "dgvMorosos";
            this.dgvMorosos.Size = new System.Drawing.Size(1025, 430);
            this.dgvMorosos.TabIndex = 2;
            // 
            // lRegistros
            // 
            this.lRegistros.AutoSize = true;
            this.lRegistros.Location = new System.Drawing.Point(12, 9);
            this.lRegistros.Name = "lRegistros";
            this.lRegistros.Size = new System.Drawing.Size(35, 13);
            this.lRegistros.TabIndex = 3;
            this.lRegistros.Text = "label1";
            // 
            // rowNombre
            // 
            this.rowNombre.FillWeight = 83.29595F;
            this.rowNombre.HeaderText = "Cliente";
            this.rowNombre.MinimumWidth = 100;
            this.rowNombre.Name = "rowNombre";
            // 
            // cFechaCompra
            // 
            this.cFechaCompra.HeaderText = "Fecha compra venta";
            this.cFechaCompra.Name = "cFechaCompra";
            // 
            // cUltimoPago
            // 
            this.cUltimoPago.HeaderText = "Ultimo Pago";
            this.cUltimoPago.Name = "cUltimoPago";
            // 
            // cFechaUltimoPago
            // 
            this.cFechaUltimoPago.HeaderText = "Fecha ultimo pago";
            this.cFechaUltimoPago.Name = "cFechaUltimoPago";
            // 
            // cPredio
            // 
            this.cPredio.HeaderText = "Predio";
            this.cPredio.Name = "cPredio";
            // 
            // cManzana
            // 
            this.cManzana.HeaderText = "Manzana";
            this.cManzana.Name = "cManzana";
            // 
            // cLote
            // 
            this.cLote.HeaderText = "Lote";
            this.cLote.Name = "cLote";
            // 
            // rowTelefono1
            // 
            this.rowTelefono1.FillWeight = 50F;
            this.rowTelefono1.HeaderText = "Telefono1";
            this.rowTelefono1.MinimumWidth = 50;
            this.rowTelefono1.Name = "rowTelefono1";
            // 
            // rowTelefono2
            // 
            this.rowTelefono2.FillWeight = 50F;
            this.rowTelefono2.HeaderText = "Telefono2";
            this.rowTelefono2.MinimumWidth = 50;
            this.rowTelefono2.Name = "rowTelefono2";
            // 
            // rowFechaCita
            // 
            this.rowFechaCita.FillWeight = 50F;
            this.rowFechaCita.HeaderText = "Fecha Cita";
            this.rowFechaCita.MinimumWidth = 50;
            this.rowFechaCita.Name = "rowFechaCita";
            // 
            // rowHora
            // 
            this.rowHora.FillWeight = 30F;
            this.rowHora.HeaderText = "Hora cita";
            this.rowHora.MinimumWidth = 30;
            this.rowHora.Name = "rowHora";
            // 
            // rowNumeroCita
            // 
            this.rowNumeroCita.FillWeight = 15F;
            this.rowNumeroCita.HeaderText = "No Cita";
            this.rowNumeroCita.MinimumWidth = 15;
            this.rowNumeroCita.Name = "rowNumeroCita";
            // 
            // FrmConsultaMorosos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 505);
            this.Controls.Add(this.lRegistros);
            this.Controls.Add(this.dgvMorosos);
            this.Controls.Add(this.cmdRealizarConsulta);
            this.Controls.Add(this.dtpFechaMorosos);
            this.Name = "FrmConsultaMorosos";
            this.Text = "Atrasados";
            ((System.ComponentModel.ISupportInitialize)(this.dgvMorosos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpFechaMorosos;
        private System.Windows.Forms.Button cmdRealizarConsulta;
        private System.Windows.Forms.DataGridView dgvMorosos;
        private System.Windows.Forms.Label lRegistros;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFechaCompra;
        private System.Windows.Forms.DataGridViewTextBoxColumn cUltimoPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFechaUltimoPago;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPredio;
        private System.Windows.Forms.DataGridViewTextBoxColumn cManzana;
        private System.Windows.Forms.DataGridViewTextBoxColumn cLote;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowTelefono1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowTelefono2;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowFechaCita;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowHora;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowNumeroCita;
    }
}