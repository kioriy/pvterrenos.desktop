﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Core;


namespace PvTerrenos
{
    public partial class FrmHistorial : Form
    {
        WSpvt.PVT ws = new WSpvt.PVT();

        public FrmHistorial()
        {
            InitializeComponent();
            llenarComboPredio();
            cargarComboBox(cbDesde, 2011);
        }

        public void llenarComboPredio()
        {
            string respuestaCargaPredio = "";

            try
            {
                respuestaCargaPredio = ws.cargaColumnaTablaPredio("nombre_predio");
            }
            catch (Exception)
            {
                MessageBox.Show("Tiempo de espera ecxedido posible problema en la red");
                //FrmVentaLote ventalote = new FrmVentaLote();
                //ventalote.Close();
            }
            string[] splitPredios = respuestaCargaPredio.Split(new char[] { ',' });

            foreach (string cargaCombo in splitPredios)
            {
                cbPredio.Items.Add(cargaCombo);
            }
        }

        private void cargarComboBox(ComboBox comboBox, int año)
        {
            comboBox.Items.Clear();

            for (int i = año; i <= DateTime.Today.Year; i++)
            {
                comboBox.Items.Add(i);
            }
        }


        private void cbPredio_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbManzana.Items.Clear();
            string idPredio = "";
            string respuestaCargaManzana = "";
            string[] splitIdManzana;

            idPredio = ws.getIdPredio(cbPredio.SelectedItem.ToString());

            respuestaCargaManzana = ws.cargaColumnaTablaManzana(idPredio, "n_manzana");

            if (respuestaCargaManzana != "")
            {
                string respuestaIdManzana = ws.cargaColumnaTablaManzana(idPredio, "id_manzana");

                splitIdManzana = respuestaIdManzana.Split(new char[] { ',' });

                string[] splitCargaManzana = respuestaCargaManzana.Split(new char[] { ',' });

                foreach (string cargaCombo in splitCargaManzana)
                {

                    cbManzana.Items.Add(cargaCombo);
                }
            }
            //else
            //{
            //    cbManzana.Items.Clear();
            //    cbLotes.Items.Clear();

            //    // string idPredio = ws.getIdPredio((string)cbPredio.SelectedItem);
            //    //string idManzana = ws.getIdManzana((string)cbManzana.SelectedItem, idPredio);
            //    string respuestaCargaLote = ws.cargaLotesDePredio(idPredio);

            //    string[] splitCargaLotes = respuestaCargaLote.Split(new char[] { ',' });
            //    int tamaño = splitCargaLotes.Length;
            //    foreach (string cargaLotes in splitCargaLotes)
            //    {
            //        cbLotes.Items.Add(cargaLotes);
            //    }
            //}
        }

        private void cbHasta_SelectedIndexChanged(object sender, EventArgs e)
        {
            int numeroColumnas = (Convert.ToInt32(cbHasta.Text) - Convert.ToInt32(cbDesde.Text) + 1) * 12;

            if (numeroColumnas == 0)
            {
                numeroColumnas = 12;
            }

            inicializarDataGridView(numeroColumnas);
        }

        private void cbDesde_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarComboBox(cbHasta, Convert.ToInt32(cbDesde.Text));
        }

        private void cmdRalizarConsulta_Click(object sender, EventArgs e)
        {
            if (cbDesde.Text != "" && cbHasta.Text != "" && cbPredio.Text != "" && cbManzana.Text != "")
            {
                dgvHistorial.Rows.Clear();

                string predio = cbPredio.Text;
                string manzana = cbManzana.Text;

                string respuestaHistorial = ws.getHistorialPagos(predio, manzana);

                string[] splitClienteDetalle = explode(respuestaHistorial, '@');

                string[] datosCliente = explode(splitClienteDetalle[0],'|');

                string[] splitDetallePago = explode(splitClienteDetalle[1], '|');

                for(int i = 0; i < datosCliente.Length; i++)
                {
                    string[] cliente = explode(datosCliente[i], ',');
                    string[] datosDetallePago = explode(splitDetallePago[i], '?');
                    string nombreComprador = cliente[0];
                    string numeroLote = cliente[1];

                    dgvHistorial.Rows.Add(nombreComprador, numeroLote);

                    for (int j = 0; j < datosDetallePago.Length-1; j++) 
                    {
                        string[] datos = explode(datosDetallePago[j], ',');

                        double montoPagoMora = Convert.ToDouble(datos[0]);
						double montoAbonoMora =  Convert.ToDouble(datos[1]);
						double montoPagoMes =  Convert.ToDouble(datos[2]);
						double montoAbonoMes =  Convert.ToDouble(datos[3]);
						string fechaProximoPago = datos[4];
						string fechaPago = Convert.ToDateTime(datos[5]).ToString("MMMM yyyy");
						string pagoActual = datos[6];

                        double sumaTotal = montoPagoMora + montoAbonoMora + montoPagoMes + montoAbonoMes;

                        for (int k = 0; k < dgvHistorial.Columns.Count; k++)
                        {
                            if (dgvHistorial.Columns[k].Name.Equals(fechaPago)) 
                            {
                                if (dgvHistorial[k, i].Value == null)
                                {
                                    dgvHistorial[k, i].Value = string.Format("{0:N2}", sumaTotal);
                                    break;
                                }
                                else
                                {
                                    double sumarDatosExtras = Convert.ToDouble(dgvHistorial[k, i].Value.ToString()) + sumaTotal;
                                    dgvHistorial[k, i].Value = string.Format("{0:N2}", sumarDatosExtras); ;
                                    break;
                                }
                            }
                        }
                    }
                }
                //dgvHistorial[2, 1].Value = "son mamadas";
            }
            else
            {
                MessageBox.Show("favor de llenar todos los campos");
            }
            cmdExportarEcxel.Enabled = true;
        }

        private double sumarTotal (double montoPagoMora, double montoAbonoMora, double montoPagoMes, double montoAbonoMes)
        {
            double total = 0.00;



            return total;
        }

        private void inicializarDataGridView(int numeroColumnas)
        {
            dgvHistorial.Rows.Clear();
            dgvHistorial.Columns.Clear();

            // Create an unbound DataGridView by declaring a column count.
            dgvHistorial.ColumnCount = numeroColumnas+2;
            dgvHistorial.ColumnHeadersVisible = true;
            dgvHistorial.Columns[0].Name = "Comprador";
            dgvHistorial.Columns[1].Name = "Lote";

            //int contador = numeroColumnas / 12;
            int j = 0;
            int añoDesde = Convert.ToInt32(cbDesde.Text);
            int añoHasta = Convert.ToInt32(cbHasta.Text);
            string[] meses = { "enero", "febrero", "marzo", "abril", "mayo", "Junio", "Julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };

            DateTime fecha = new DateTime(añoHasta, 01, 01);

            // Set the column header style.
            DataGridViewCellStyle columnHeaderStyle = new DataGridViewCellStyle();

            columnHeaderStyle.BackColor = Color.Beige;
            columnHeaderStyle.Font = new Font("Verdana", 8, FontStyle.Regular);
            dgvHistorial.ColumnHeadersDefaultCellStyle = columnHeaderStyle;

            // Set the column header names.
            for (int i = 2; i < numeroColumnas+2; i++)
            {
                dgvHistorial.Columns[i].Name = meses[j] + " " + añoDesde;

                j++;

                if (j == 12)
                {
                    j = 0;
                    añoDesde++;
                }
            }
            //dgvHistorial.Columns[0].Name = "Recipe";
            //dgvHistorial.Columns[1].Name = "Category";
            //dgvHistorial.Columns[2].Name = "Main Ingredients";
            //dgvHistorial.Columns[3].Name = "Rating";

            //// Populate the rows.
            //string[] row1 = new string[] { "Meatloaf", "Main Dish", "ground beef",
            //"**" };
            //string[] row2 = new string[] { "Key Lime Pie", "Dessert", 
            //"lime juice, evaporated milk", "****" };
            //string[] row3 = new string[] { "Orange-Salsa Pork Chops", "Main Dish", 
            //"pork chops, salsa, orange juice", "****" };
            //string[] row4 = new string[] { "Black Bean and Rice Salad", "Salad", 
            //"black beans, brown rice", "****" };
            //string[] row5 = new string[] { "Chocolate Cheesecake", "Dessert", 
            //"cream cheese", "***" };
            //string[] row6 = new string[] { "Black Bean Dip", "Appetizer", 
            //"black beans, sour cream", "***" };
            //object[] rows = new object[] { row1, row2, row3, row4, row5, row6 };

            //foreach (string[] rowArray in rows)
            //{
            //    dgvHistorial.Rows.Add(rowArray);
            //}
        }

        private string[] explode(string tokens, char delimitador) 
        {
            string[] split= tokens.Split(new char[] {delimitador});

            return split;
        }

        private void ExportarDataGridViewExcel(DataGridView dgv)
        {
            SaveFileDialog fichero = new SaveFileDialog();
            fichero.Filter = "Excel (*.xls)|*.xls";

            if (fichero.ShowDialog() == DialogResult.OK)
            {
                Microsoft.Office.Interop.Excel.Application aplicacion;
                Microsoft.Office.Interop.Excel.Workbook libros_trabajo;
                Microsoft.Office.Interop.Excel.Worksheet hoja_trabajo;

                aplicacion = new Microsoft.Office.Interop.Excel.Application();
                libros_trabajo = aplicacion.Workbooks.Add();
                hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                

                //Recorremos el DataGridView rellenando la hoja de trabajo
                for (int i = 0; i < dgv.Rows.Count - 1; i++)
                {
                    //hoja_trabajo.Cells[1, i + 1] = dgv.Columns[i].Name.ToString();

                    for (int j = 0; j < dgv.Columns.Count; j++)
                    {
                        hoja_trabajo.Cells[1, j + 1] = dgv.Columns[j].Name.ToString();
                        
                        if (dgv.Rows[i].Cells[j].Value == null)
                        {
                            hoja_trabajo.Cells[i + 2, j + 1] = "";
                        }
                        else
                        {
                            hoja_trabajo.Cells[i + 2, j + 1] = dgv.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                }
                hoja_trabajo.Columns.AutoFit();
                hoja_trabajo.Rows.HorizontalAlignment = 3;
                libros_trabajo.SaveAs(fichero.FileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                libros_trabajo.Close(true);
                aplicacion.Quit();
            }
        } 

        private void cmdExportarEcxel_Click(object sender, EventArgs e)
        {
            ExportarDataGridViewExcel(dgvHistorial);
        }
    }
}
