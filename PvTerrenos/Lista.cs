﻿public enum NivelAcceso
{
    ADMINISTRADOR,
    ADMINISTRADORMM,
    SUPERVISOR,
    CAJERO,
    COBRANZA,
    VENDEDOR,
    SISTEMAS 
}

public enum action
{
    insert,
    query,
    update,
    free
}

public enum messageResponse
{
    conectionFail,
    actionFail,
    actionSuccess,
    fieldsRequired,
    allActionFail,
}