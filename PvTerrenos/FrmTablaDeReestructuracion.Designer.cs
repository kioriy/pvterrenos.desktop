﻿namespace PvTerrenos
{
    partial class FrmTablaDeReestructuracion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvPagado = new System.Windows.Forms.DataGridView();
            this.rowMes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowMensualidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowNumeroPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvDeuda = new System.Windows.Forms.DataGridView();
            this.rowMesEnDeuda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowInteres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowMensualidadProximoPago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtSubDivision = new System.Windows.Forms.TextBox();
            this.lAumentoMensual = new System.Windows.Forms.Label();
            this.lSubDivision = new System.Windows.Forms.Label();
            this.lTres = new System.Windows.Forms.Label();
            this.lPaso3 = new System.Windows.Forms.Label();
            this.lRestructuracion = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lDeudaTotal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.lPagado = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAumentoMensual = new System.Windows.Forms.TextBox();
            this.cmdAceptar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPagado)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeuda)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvPagado
            // 
            this.dgvPagado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPagado.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPagado.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPagado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPagado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rowMes,
            this.rowMensualidad,
            this.rowNumeroPago});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPagado.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPagado.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.dgvPagado.Location = new System.Drawing.Point(8, 19);
            this.dgvPagado.Name = "dgvPagado";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPagado.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvPagado.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPagado.Size = new System.Drawing.Size(371, 421);
            this.dgvPagado.TabIndex = 0;
            // 
            // rowMes
            // 
            this.rowMes.FillWeight = 150F;
            this.rowMes.HeaderText = "Mes";
            this.rowMes.Name = "rowMes";
            // 
            // rowMensualidad
            // 
            this.rowMensualidad.HeaderText = "Mensualidad";
            this.rowMensualidad.Name = "rowMensualidad";
            // 
            // rowNumeroPago
            // 
            this.rowNumeroPago.HeaderText = "Numero pago";
            this.rowNumeroPago.Name = "rowNumeroPago";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvPagado);
            this.groupBox1.Location = new System.Drawing.Point(5, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 449);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pagos realizados";
            // 
            // dgvDeuda
            // 
            this.dgvDeuda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDeuda.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDeuda.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvDeuda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeuda.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rowMesEnDeuda,
            this.rowInteres,
            this.rowMensualidadProximoPago});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDeuda.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvDeuda.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.dgvDeuda.Location = new System.Drawing.Point(8, 19);
            this.dgvDeuda.Name = "dgvDeuda";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDeuda.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvDeuda.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvDeuda.Size = new System.Drawing.Size(371, 421);
            this.dgvDeuda.TabIndex = 0;
            // 
            // rowMesEnDeuda
            // 
            this.rowMesEnDeuda.FillWeight = 150F;
            this.rowMesEnDeuda.HeaderText = "Mes en deuda";
            this.rowMesEnDeuda.Name = "rowMesEnDeuda";
            // 
            // rowInteres
            // 
            this.rowInteres.HeaderText = "Interes";
            this.rowInteres.Name = "rowInteres";
            // 
            // rowMensualidadProximoPago
            // 
            this.rowMensualidadProximoPago.HeaderText = "Mensualidad";
            this.rowMensualidadProximoPago.Name = "rowMensualidadProximoPago";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvDeuda);
            this.groupBox2.Location = new System.Drawing.Point(396, 81);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(385, 449);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Deuda actual";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView2);
            this.groupBox3.Location = new System.Drawing.Point(787, 81);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(385, 449);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Reestructuracion";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.ButtonShadow;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView2.GridColor = System.Drawing.SystemColors.AppWorkspace;
            this.dataGridView2.Location = new System.Drawing.Point(8, 19);
            this.dataGridView2.Name = "dataGridView2";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView2.Size = new System.Drawing.Size(371, 421);
            this.dataGridView2.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.FillWeight = 150F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Mes en deuda";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Interes";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Mensualidad";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.panel1.Controls.Add(this.txtSubDivision);
            this.panel1.Controls.Add(this.cmdAceptar);
            this.panel1.Controls.Add(this.txtAumentoMensual);
            this.panel1.Controls.Add(this.lAumentoMensual);
            this.panel1.Controls.Add(this.lSubDivision);
            this.panel1.Controls.Add(this.lTres);
            this.panel1.Controls.Add(this.lPaso3);
            this.panel1.Controls.Add(this.lRestructuracion);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.lDeudaTotal);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lPagado);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(-1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1267, 75);
            this.panel1.TabIndex = 9;
            // 
            // txtSubDivision
            // 
            this.txtSubDivision.Location = new System.Drawing.Point(773, 39);
            this.txtSubDivision.Name = "txtSubDivision";
            this.txtSubDivision.Size = new System.Drawing.Size(100, 20);
            this.txtSubDivision.TabIndex = 18;
            this.txtSubDivision.Visible = false;
            // 
            // lAumentoMensual
            // 
            this.lAumentoMensual.AutoSize = true;
            this.lAumentoMensual.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lAumentoMensual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lAumentoMensual.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lAumentoMensual.Location = new System.Drawing.Point(637, 40);
            this.lAumentoMensual.Name = "lAumentoMensual";
            this.lAumentoMensual.Size = new System.Drawing.Size(130, 16);
            this.lAumentoMensual.TabIndex = 17;
            this.lAumentoMensual.Text = "Aumento mensual";
            this.lAumentoMensual.Visible = false;
            // 
            // lSubDivision
            // 
            this.lSubDivision.AutoSize = true;
            this.lSubDivision.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lSubDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lSubDivision.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lSubDivision.Location = new System.Drawing.Point(665, 41);
            this.lSubDivision.Name = "lSubDivision";
            this.lSubDivision.Size = new System.Drawing.Size(102, 16);
            this.lSubDivision.TabIndex = 16;
            this.lSubDivision.Text = "Sub - division";
            this.lSubDivision.Visible = false;
            // 
            // lTres
            // 
            this.lTres.AutoSize = true;
            this.lTres.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lTres.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTres.ForeColor = System.Drawing.Color.Black;
            this.lTres.Location = new System.Drawing.Point(731, 3);
            this.lTres.Name = "lTres";
            this.lTres.Size = new System.Drawing.Size(21, 24);
            this.lTres.TabIndex = 15;
            this.lTres.Text = "3";
            this.lTres.Visible = false;
            // 
            // lPaso3
            // 
            this.lPaso3.AutoSize = true;
            this.lPaso3.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lPaso3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lPaso3.ForeColor = System.Drawing.Color.Black;
            this.lPaso3.Location = new System.Drawing.Point(669, 3);
            this.lPaso3.Name = "lPaso3";
            this.lPaso3.Size = new System.Drawing.Size(66, 24);
            this.lPaso3.TabIndex = 14;
            this.lPaso3.Text = "PASO";
            this.lPaso3.Visible = false;
            // 
            // lRestructuracion
            // 
            this.lRestructuracion.AutoSize = true;
            this.lRestructuracion.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lRestructuracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lRestructuracion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lRestructuracion.Location = new System.Drawing.Point(762, 8);
            this.lRestructuracion.Name = "lRestructuracion";
            this.lRestructuracion.Size = new System.Drawing.Size(38, 16);
            this.lRestructuracion.TabIndex = 13;
            this.lRestructuracion.Text = "aqui";
            this.lRestructuracion.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.CornflowerBlue;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(412, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 24);
            this.label5.TabIndex = 12;
            this.label5.Text = "2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.CornflowerBlue;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(350, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 24);
            this.label6.TabIndex = 11;
            this.label6.Text = "PASO";
            // 
            // lDeudaTotal
            // 
            this.lDeudaTotal.AutoSize = true;
            this.lDeudaTotal.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lDeudaTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDeudaTotal.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lDeudaTotal.Location = new System.Drawing.Point(364, 41);
            this.lDeudaTotal.Name = "lDeudaTotal";
            this.lDeudaTotal.Size = new System.Drawing.Size(38, 16);
            this.lDeudaTotal.TabIndex = 10;
            this.lDeudaTotal.Text = "aqui";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(80, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "1";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(115, 6);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(171, 18);
            this.dateTimePicker1.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.CornflowerBlue;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(18, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "FECHA COMPRA";
            // 
            // lPagado
            // 
            this.lPagado.AutoSize = true;
            this.lPagado.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lPagado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lPagado.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lPagado.Location = new System.Drawing.Point(107, 41);
            this.lPagado.Name = "lPagado";
            this.lPagado.Size = new System.Drawing.Size(38, 16);
            this.lPagado.TabIndex = 6;
            this.lPagado.Text = "aqui";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.CornflowerBlue;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(18, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "PASO";
            // 
            // txtAumentoMensual
            // 
            this.txtAumentoMensual.Location = new System.Drawing.Point(773, 39);
            this.txtAumentoMensual.Name = "txtAumentoMensual";
            this.txtAumentoMensual.Size = new System.Drawing.Size(100, 20);
            this.txtAumentoMensual.TabIndex = 19;
            this.txtAumentoMensual.Visible = false;
            // 
            // cmdAceptar
            // 
            this.cmdAceptar.Location = new System.Drawing.Point(879, 38);
            this.cmdAceptar.Name = "cmdAceptar";
            this.cmdAceptar.Size = new System.Drawing.Size(73, 22);
            this.cmdAceptar.TabIndex = 20;
            this.cmdAceptar.Text = "Aceptar";
            this.cmdAceptar.UseVisualStyleBackColor = true;
            this.cmdAceptar.Visible = false;
            this.cmdAceptar.Click += new System.EventHandler(this.cmdAceptar_Click);
            // 
            // TablaDeReestructuracion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1176, 536);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "TablaDeReestructuracion";
            this.Text = "Tabla de reestructuracion";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPagado)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeuda)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPagado;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMes;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMensualidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowNumeroPago;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvDeuda;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMesEnDeuda;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowInteres;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowMensualidadProximoPago;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lPagado;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lDeudaTotal;
        private System.Windows.Forms.TextBox txtSubDivision;
        private System.Windows.Forms.Label lAumentoMensual;
        private System.Windows.Forms.Label lSubDivision;
        private System.Windows.Forms.Label lTres;
        private System.Windows.Forms.Label lPaso3;
        private System.Windows.Forms.Label lRestructuracion;
        private System.Windows.Forms.Button cmdAceptar;
        private System.Windows.Forms.TextBox txtAumentoMensual;
    }
}