﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace PvTerrenos
{
    public partial class FrmCorteCaja : Form
    {
        PdfCreate recibo = new PdfCreate();
        WSpvt.PVT ws = new WSpvt.PVT();

        string usuario = "";
        string nivelUsuario = "";
        int idUsuario = 0;
        string administracion = "";

        public FrmCorteCaja(string usuario, string nivelUsuario, int idUsuario, string administracion)
        {
            InitializeComponent();

            this.usuario = usuario;
            this.nivelUsuario = nivelUsuario ;
            this.idUsuario = idUsuario;
            this.administracion = administracion;

            inicializarPermisos();
        }

        ////pagos
        private void cmdRealizarConsultas_Click_1(object sender, EventArgs e)
        {
            dgvConsultas.Rows.Clear();
            cargarTabla(dgvConsultas, "1", txtTotalPagado, "Administracion General");
            cargarTabla(dgvConsultasMM, "1", txtTotalPagodoMM, "M & M");
        }

        private void cmdConsultaFecha_Click_1(object sender, EventArgs e)
        {
            string fecha = dtpFechaConsulta.Value.ToShortDateString();
            string consulta = "";
            string consultaMM = "";
            txtTotalPagado.Text = "0";
            txtTotalPagodoMM.Text = "0";
            //string mostrarSoloAsociados = "";
            if (nivelUsuario != NivelAcceso.ADMINISTRADORMM.ToString())
            {
                //mostrarSoloAsociados = "AND `administracion` = '" + administracion + "'";
                consulta = "r.`nombreUsuario` = '" + cbEmpleado.Text + "' AND STR_TO_DATE( `fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')" + "AND `administracion` = 'GENERAL'";
                cargarTabla(dgvConsultas, consulta, txtTotalPagado, "Administracioin General");           
            }
            if(nivelUsuario == NivelAcceso.CAJERO.ToString() || nivelUsuario == NivelAcceso.ADMINISTRADOR.ToString() || nivelUsuario == NivelAcceso.SUPERVISOR.ToString())
            {
                consultaMM = "r.`nombreUsuario` = '" + cbEmpleado.Text + "' AND STR_TO_DATE( `fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')" + "AND `administracion` = 'MM'";
                cargarTabla(dgvConsultasMM, consultaMM, txtTotalPagodoMM, "M & M");
            }
            //string fecha = dtpFechaConsulta.Value.ToString("dd/MM/yyyy 12:00:00 "/*18/11/2014 12:00:00 a.m.*/);
            //string fechaConcatenada = fecha + "a.m.";
            //`idDetallePago` IN (SELECT `idActividad` FROM `RegistroActividad` WHERE `nombreUsuario` = 'yeni')
            //string consulta = "STR_TO_DATE( `fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')";
            
            //string consulta = "WHERE r.`nombreUsuario` = '" + cbEmpleado.Text + "' AND STR_TO_DATE( a.`fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')";
            //string consulta = "WHERE r.`nombreUsuario` = '" + cbEmpleado.Text + "') AND STR_TO_DATE( a.`fechaPago` , '%d/%m/%Y' ) = STR_TO_DATE( '" + fecha + "', '%d/%m/%Y')";

            //dgvConsultas.Rows.Clear();
            
            double sumaAdministraciones = Convert.ToDouble(txtTotalPagado.Text) + Convert.ToDouble(txtTotalPagodoMM.Text);
            txtTotal.Text = string.Format("{0:N2}",sumaAdministraciones);
        }

        private void cmdReporte_Click_1(object sender, EventArgs e)
        {
            if (dgvConsultas.Rows.Count > 1) 
            {
                reporte(dgvConsultas, txtTotalPagado);
            }

            if (dgvConsultasMM.Rows.Count > 1) 
            {
                reporte(dgvConsultasMM, txtTotalPagodoMM);
            }
        }

        //METODOS**
        public void cargarTabla(DataGridView tabla, string consulta, TextBox sumarTotal, string TipoAdministracion)
        {
            tabla.Rows.Clear();

            string respuestaConsulta = "";
            int bandera = 0;
            try
            {
                ws.Timeout = 600000;
                respuestaConsulta = ws.getDetallePago(consulta);
            }
            catch (WebException e) 
            { 
                MessageBox.Show(e.Message); 
            }

            string[] splitDatos = respuestaConsulta.Split(new char[] { '|' });

            if (splitDatos[0] != "" && splitDatos[0] != "0")
            {
                foreach (string datosInsertar in splitDatos)
                {
                    string[] splitDatosPago = datosInsertar.Split(new char[] { ',' });

                    string comprador = splitDatosPago[0];

                    //string[] sinSimbolo = splitDatosPago[1].Split(new char[] { '$' });
                    //double montoPagoMora = Convert.ToDouble(sinSimbolo[1].Trim());
                    double montoPagoMora = Convert.ToDouble(splitDatosPago[1].Trim());
                    double montoAbonoMora = Convert.ToDouble(splitDatosPago[2].Trim());
                    double montoPagoMes = Convert.ToDouble(splitDatosPago[3].Trim());
                    double montoAbonoMes = Convert.ToDouble(splitDatosPago[4].Trim());

                    double total = montoAbonoMora + montoAbonoMes + montoPagoMes + montoPagoMora;

                    string totalPagado = string.Format("{0:N2}", total);

                    string numeroLote = splitDatosPago[5];
                    string numeroManzana = splitDatosPago[6];
                    string fechaProximoPago = Convert.ToDateTime(splitDatosPago[7]).ToString("MMMM - yyyy");
                    string fechaPago = Convert.ToDateTime(splitDatosPago[8]).ToString("dd/MM/yy hh:mm:ss");
                    string nombrePredio = splitDatosPago[9];

                    tabla.Rows.Insert(0, comprador, totalPagado, numeroLote, numeroManzana, fechaProximoPago, fechaPago, nombrePredio);
                    bandera++;
                }
                sumarTotalPagado(tabla, sumarTotal);
            }
            else 
            {
                MessageBox.Show("La consulta no genero ningun resultado para " + TipoAdministracion + "");   
            }
        }

        private void sumarTotalPagado(DataGridView tabla, TextBox totalPagado)
        {
            double suma = 0;

            foreach (DataGridViewRow row in tabla.Rows)
            {
                if (row.Cells[2].Value == DBNull.Value)
                    continue;

                double valorcell = 0;
                double.TryParse(Convert.ToString(row.Cells[1].Value), out valorcell);

                suma += Convert.ToDouble(valorcell);
            }

            totalPagado.Text = string.Format("{0:N2}", suma);
        }

        private void inicializarPermisos() 
        {
            if (nivelUsuario == NivelAcceso.CAJERO.ToString()) 
            {
                cmdMostrarTodo.Enabled = false;
                cmdReporte.Enabled = false;

                int index = cbEmpleado.FindString(usuario);

                if (index >= 0) 
                {
                    cbEmpleado.SelectedIndex = index;

                    cbEmpleado.Enabled = false;
                    dtpFechaConsulta.Enabled = false;
                }
            }
        }

        private void reporte(DataGridView tabla, TextBox totalPagado) 
        {
            DataTable datosTabla = new DataTable();
            DataRow filaNueva;
            string fechaConsulta = dtpFechaConsulta.Value.ToString("dd-MMMM-yyyy");
            string totalConsulta = totalPagado.Text;

            if (tabla[0, 0].Value.ToString() == string.Empty)
            {
                MessageBox.Show("Favor de llenar la tabla con los datos de la consulta");
            }
            else
            {
                //foreach (DataColumn columna in dgvConsultas.Columns) 
                //{
                //    DataColumn columnaNueva = new DataColumn(columna.ColumnName);

                //    datosTabla.Columns.Add(columnaNueva);
                //}

                datosTabla.Columns.Add("Comprador");
                datosTabla.Columns.Add("Paga");
                datosTabla.Columns.Add("Lote");
                datosTabla.Columns.Add("Manzana");
                datosTabla.Columns.Add("Mes pagado");
                datosTabla.Columns.Add("Fecha de pago");
                datosTabla.Columns.Add("Predio");

                foreach (DataGridViewRow filaDatos in tabla.Rows)
                {
                    filaNueva = datosTabla.NewRow();

                    //for (int i = 0; dgvConsultas.Rows.ToString() != string.Empty; i++) 
                    //{
                    filaNueva["Comprador"] = filaDatos.Cells[0].Value;
                    filaNueva["Paga"] = filaDatos.Cells[1].Value;
                    filaNueva["Lote"] = filaDatos.Cells[2].Value;
                    filaNueva["Manzana"] = filaDatos.Cells[3].Value;
                    filaNueva["Mes pagado"] = filaDatos.Cells[4].Value;
                    filaNueva["Fecha de pago"] = filaDatos.Cells[5].Value;
                    filaNueva["predio"] = filaDatos.Cells[6].Value;

                    datosTabla.Rows.Add(filaNueva);
                    //}
                }

                //datosTabla.TableName = "consulta";

                //recibo.reporteDiario(datosTabla, fechaConsulta, totalConsulta);
                recibo.reporteDiarioDos(datosTabla, fechaConsulta, totalConsulta);
            }
        }
    }
}
