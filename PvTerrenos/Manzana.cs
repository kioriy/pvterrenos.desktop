﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace PvTerrenos
{
    class Manzana : CRUD
    {
        public string id_manzana { get; set; }
        public string n_manzana { get; set; }
        public string fecha_creacion { get; set; }
        public string pk_predio { get; set; }

        private string table = "";

        public List<Manzana> _list = new List<Manzana>();

        public Manzana()
        {
            table = MANZANA ;
        }

        #region METODOS
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }

        public string idManzana(string n_manzana, string id_predio)
        {
            if (string.IsNullOrEmpty(n_manzana))
            {
                return "0";
            }
            else
            {
                return _list.Find(x => x.n_manzana == n_manzana && x.pk_predio == id_predio).id_manzana;
            }
        }

        public string numeroManzana(string id_manzana)
        {
            if (string.IsNullOrEmpty(id_manzana) || id_manzana == "0")
            {
                return "0";
            }
            else
            {
                return _list.Find(x => x.id_manzana == id_manzana).n_manzana;
            }
        }

        public void loadLlist()
        {
            _list = list<Manzana>();
        }
        #endregion

        #region METODOS INTERFACE
        public void loadComboBox(ComboBox cb, string id_predio)
        {
            //query("1", $"{WHERE} ? ORDER BY `n_manzana` ASC");

            var numero_manzana = _list.FindAll(x => x.pk_predio == id_predio);

            if(numero_manzana.Count != 0)
            {
                foreach (var manzana in numero_manzana)
                {
                    cb.Items.Add(manzana.n_manzana.ToUpper());
                }
            }  
        }
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"{n_manzana.ToLower().Trim()}{cc}" +
            $"{fecha_creacion.ToUpper().Trim()}{cc}" +
            $"{pk_predio.ToUpper().Trim()}";
        }
        #endregion
    }
}
