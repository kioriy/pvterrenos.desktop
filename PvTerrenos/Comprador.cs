﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace PvTerrenos
{
    public class Comprador : CRUD
    {
        public string id_comprador { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string colonia { get; set; }
        public string municipio { get; set; }
        public string beneficiario { get; set; }
        public string sexo { get; set; }
        public string originario { get; set; }
        public string residencia { get; set; }
        public string ocupacion { get; set; }
        public string estado_civil { get; set; }
        public string tel1 { get; set; }
        public string tel2 { get; set; }

        public List<string> _listaIdComprador = new List<string>();
        public List<Comprador> _list;

        public string table = "";
        //private string json = "";

        //public List<Comprador> comprador_list { get; set; }

        public Comprador()
        {
            table = COMPRADOR;
        }

        #region METODOS
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        } 
         
        public void inicializarlistaId()
        {
            _listaIdComprador.Clear();

            _listaIdComprador = list<Comprador>().Select(x => x.id_comprador).ToList();

            //foreach (var comprador in list<Comprador>())
            //{
            //    _listaIdComprador.Add(comprador.id_comprador);
            //}
        }

        public string idComprador(int index)
        { 
            string idComprador = _listaIdComprador[index];

            return _listaIdComprador[index];
            //return list<Comprador>().Find(x => x.id_comprador == nombre).id_comprador;
        }

        public int load(string id_comprador)
        {
            return list<Comprador>().FindIndex(x => x.id_comprador == id_comprador);
        }

        public void insertLastId()
        {
            int index = _list.Count() - 2;

            string penultimoId = _list[index].id_comprador;

            string ultimoId = Convert.ToString(Convert.ToInt32(penultimoId) + 1);

            _list.Last().id_comprador = ultimoId;
                      _listaIdComprador.Add(ultimoId);
        }

        public void loadList()
        {
            _list = list<Comprador>();
        }
        #endregion

        #region METODOS INTERFACE
        public void loadComboBox(ComboBox cb)
        {
            cb.Items.Clear();

            //query("1", $"WHERE ? ORDER BY `id_user` ASC");

            foreach (Comprador comprador in _list)
            {
                cb.Items.Add(comprador.nombre.ToUpper());
            }
        }
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"0{cc}" +
            $"{nombre.ToUpper().Trim()}{cc}" +
            $"{direccion.ToUpper().Trim()}{cc}" +
            $"{colonia.ToUpper().Trim()}{cc}" +
            $"{municipio.ToUpper().Trim()}{cc}" +
            $"{beneficiario}{cc}" +
            $"{sexo.ToUpper().Trim()}{cc}" +
            $"{originario.ToUpper().Trim()}{cc}" +
            $"{residencia.ToUpper().Trim()}{cc}" +
            $"{ocupacion.ToUpper().Trim()}{cc}" +
            $"{estado_civil.ToUpper().Trim()}{cc}" +
            $"{tel1.ToUpper().Trim()}{cc}" +
            $"{tel2.ToUpper().Trim()}";
        }
        #endregion
    }
}
  