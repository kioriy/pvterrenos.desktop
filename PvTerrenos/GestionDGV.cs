﻿   using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    class GestionDGV
    {
        #region variables;
        private DataGridView dgv;
        #endregion

        #region constructor
        public GestionDGV(DataGridView dgv)
        {
            this.dgv = dgv;
        }

        public GestionDGV()
        {
        }
        #endregion

        #region METODOS
        public void inicializarDgv()
        {
            foreach (DataGridViewColumn col in this.dgv.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Font = new Font("Arial", 16F, FontStyle.Bold, GraphicsUnit.Pixel);
            }

            //DataGridViewRow row = this.dgv.Rows[0];
            //row.Height = 35;

            this.dgv.RowsDefaultCellStyle.BackColor = Color.LightBlue;
            this.dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.White;

            dgv.Columns[1].Width = 90;
            dgv.Columns[2].Width = 220;
            dgv.Columns[3].Width = 90;
            dgv.Columns[4].Width = 90;
        }

        public void loadDgvPagos(List<DetallePago> _list, DataGridView dgv)
        {
            dgv.Rows.Clear();

            foreach (var _detallePago in _list)
            {
                dgv.Rows.Add
                    (
                        "Eliminar",
                        _detallePago.idDetallePago,
                        _detallePago.pagoActual,
                        string.Format("{0:N2}", Convert.ToDecimal(_detallePago.mensualidad)),//string.Format("{0:N0}",_detallePago.mensualidad),//_detallePago.monto,
                        _detallePago.tipo_pago,
                        Convert.ToDateTime(_detallePago.fechaProximoPago).ToString("MMMM - yyyy"),
                        _detallePago.fechaPago
                    );
            }
        }
            
        #endregion
    }
}
