﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace PvTerrenos
{
    public class Lote : CRUD
    {
        public int id_lote { get; set; }
        public string n_lote { get; set; }
        public string status_lote { get; set; }
        public string medidaNorte { get; set; }
        public string medidaSur { get; set; }
        public string medidaEste { get; set; }
        public string medidaOeste { get; set; }
        public string fecha_creacion { get; set; }
        public string pk_manzana { get; set; }
        public string pk_predio { get; set; }

        public string table = "";
        public List<Lote> _list = new List<Lote>();
   
        public Lote()
        {
            table = LOTE;
        }

        #region METODOS
        #region CRUD
        public void insert()
        {
            execute(table, values(), action.insert.ToString(), "");
        }

        public void query(string valuesString, string where)
        {
            execute(table, $"{valuesString}", action.query.ToString(), where);
        }

        public void update(string where)
        {
            execute(table, values(), action.update.ToString(), where);
        }
        #endregion

        public int idLote(string n_lote, string pk_predio, string pk_manzana)
        {
            return _list.Find
                (
                    x => x.n_lote == n_lote &&
                    x.pk_predio == pk_predio &&
                    x.pk_manzana == pk_manzana
                ).id_lote;
        }

        public string numeroLote(int id_lote)
        {
            return _list.Find(x => x.id_lote == id_lote).n_lote;
        }

        public int index(string _idPredio, string _idManzana, string _nLote)
        {
            return _list.IndexOf
                    (
                        (   //devuelve el objeto
                            _list.Find
                            (
                                x => x.pk_predio == _idPredio &&
                                x.pk_manzana == _idManzana && x.n_lote == _nLote
                            )
                        )
                    );
        }

        public void loadList()
        {
            _list = list<Lote>();
        }
        #endregion

        #region METODOS INTERFACE
        public void loadComboBox(ComboBox cb, string id_predio, string id_manzana)
        {
            //query("1", $"{WHERE} ? ORDER BY `n_lote` ASC");

            var numero_lote = _list.FindAll
                (
                    x => x.pk_predio == id_predio && 
                    x.pk_manzana == id_manzana &&
                    x.status_lote == "0"
                );

            if (numero_lote.Count != 0)
            {
                foreach (var lote in numero_lote)
                {
                    cb.Items.Add(lote.n_lote.ToUpper());
                }
            }
        }

        public bool cambioDeLote(int _loteAnterior, int _loteActual)
        {
            if (_loteAnterior >= 0)
            {
                if (_loteAnterior != _loteActual)
                {
                    return true;
                }
            }

            return false;
        }

        public void status(int index, string status)
        {
            _list[index].status_lote = status;

            _list[index].update
                (
                    queryUpdate(id_lote_a, _list[index].id_lote.ToString())
                );
        }
        #endregion

        #region VALUES
        public string values()
        {
            return
            $"{n_lote.ToLower().Trim()}{cc}" +
            $"{pk_predio.ToUpper().Trim()}{cc}" +
            $"{pk_manzana.ToUpper().Trim()}{cc}" +
            $"{status_lote.ToLower().Trim()}{cc}" +
            $"{medidaNorte}{cc}" +
            $"{medidaSur.ToUpper().Trim()}{cc}" +
            $"{medidaEste.ToUpper().Trim()}{cc}" +
            $"{medidaOeste.ToUpper().Trim()}";
            //$"{fecha_creacion.ToUpper().Trim()}{cc}" +
        }
        #endregion
    }
}
