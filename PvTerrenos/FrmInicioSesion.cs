﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml;
using PvTerrenos.WSpvt;

namespace PvTerrenos
{
    public partial class frmInicioSesion : Form
    {
        User user;

        public frmInicioSesion()
        {
            InitializeComponent();
        }

        private void btnInicioSesion_Click(object sender, EventArgs e)
        {
            loginStart();
        }

        private void txtContraseña_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                loginStart();
            }
        }

        private void loginStart()
        {
            user = new User();

            if (!ValidateForm.empty(this))
            {
                try
                {
                    user.values_list.Add(txtUsuario.Text);
                    user.values_list.Add(txtContraseña.Text);

                    string queryUser = $"{user.WHERE} {user.username_a} {user.igual} ? " +
                                       $"{user.AND} {user.password_a} {user.igual} ?";

                    user.query(user.implode(user.values_list), queryUser);

                    user.loadList();

                    if (Mensaje.result_bool)
                    {
                        user._list[0].login();

                       
                        //FrmVentaLote venta_lote = new FrmVentaLote(user._list[0]);
                        //venta_lote.Show();

                        //user.list<User>()[0].login();

                        Clear.clearForm(this);
                        this.Hide();
                    }
                    else
                    {
                        Mensaje.responseMessage("Usuario no encontrado", "Fallo inicio de sesión", false);
                    }  
                }
                catch (NullReferenceException)
                {
                    MessageBox.Show(Mensaje.result);
                }
            }
            else
            {
                MessageBox.Show(Mensaje.result);
            }
        }

        /*private void loginStart()
        {
            string nombreUsuario = this.txtUsuario.Text;
            string contraseña = this.txtContraseña.Text;
            PVT pVT = new PVT();

            string respuesta = "";

            try
            {
                pVT.Timeout = 600000;
                respuesta = pVT.login(nombreUsuario, contraseña);
            }
            catch (Exception)
            {
            }
            if (respuesta != "USUARIO NO REGISTRADO")
            {
                string[] array = respuesta.Split(new char[]	{','});
                string usuario = "";
                string nivelUsuario = "";
                string idUsuario = "";
                string administracion = "";

                if (array.Length == 4)
                {
                    usuario = array[0];
                    nivelUsuario = array[1];
                    idUsuario = array[2];
                    administracion = array[3];
                }
                else
                {
                    MessageBox.Show("El inicio de usuario no ha regresado los parametros correctos");
                }
                if (nivelUsuario == "0" || nivelUsuario == "1" || nivelUsuario == "4")
                {
                    FrmVentaLote frmVentaLote = new FrmVentaLote(user/*usuario, nivelUsuario, idUsuario, administracion);
                    frmVentaLote.Show();
                    this.txtUsuario.Text = "";
                    this.txtContraseña.Text = "";

                    this.Hide();
                }
                if (nivelUsuario == "3")
                {
                    FrmPago frmPago = new FrmPago(user/*usuario, nivelUsuario, idUsuario, administracion);
                    frmPago.Show();
                    this.txtUsuario.Text = "";
                    this.txtContraseña.Text = "";
                    this.Hide();
                }
            }
            else
            {
                MessageBox.Show(respuesta);
            }
        }*/
    }
}
