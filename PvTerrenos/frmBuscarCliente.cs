﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public partial class FrmBuscarCliente : Form
    {
        WSpvt.PVT ws = new WSpvt.PVT();
        public delegate void pasar(string nombre);
        public event pasar recibir;

        public FrmBuscarCliente()
        {
            InitializeComponent();
        }

        private void cmdBuscar_Click(object sender, EventArgs e)
        {
            string comprador = this.ws.buscarComprador(this.txtNombre.Text);
            string[] splitComprador = comprador.Split(new char[] {','});
            dgvBusqueda.Rows.Clear();

            if (splitComprador[0] == "")
            {
                MessageBox.Show("No se encontro ninguna coincidencia");
                return;
            }
            //string[] array2 = splitComprador;

            for (int i = 0; i < splitComprador.Length; i++)
            {
                string addNombre = splitComprador[i];

                dgvBusqueda.Rows.Insert(0, addNombre);
            }
        }

        private void dgvBusqueda_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.Hide();
            string nombre = this.dgvBusqueda.CurrentCell.Value.ToString();
            
            recibir(nombre);
            
            this.Close();
        }
    }
}
