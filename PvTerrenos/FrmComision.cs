﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvTerrenos
{
    public partial class FrmComision : Form
    {
        Comision comision = new Comision();
        CompraVenta _compraVeta = new CompraVenta();

        User user = new User();

        string status = "";

        public FrmComision(User user, CompraVenta _compraVenta)
        {
            InitializeComponent();
            this.user = user;
            this._compraVeta = _compraVeta;
            comision.queryFree("", comision.query_comisiones);
            comision.loadList();
            //user.loadList();
            user.loadComboBox(cbVendedor, true);
            cargarDgv();
        }

        private void cbVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*dgvComisiones.Columns.Add("cPredio", "Predio");
            dgvComisiones.Columns.Add("cPredio", "Manzana");
            dgvComisiones.Columns.Add("cPredio", "Lote");
            dgvComisiones.Columns.Add("cPredio", "Predio");*/

            try
            {
                dgvComisiones.Rows.Clear();

                int id_user = user._list.Find(x => x.nombre == cbVendedor.Text.ToLower()).id_user;

                foreach (var row in comision._list.FindAll(x => x.fk_user == id_user))
                {
                    status = validarStatusComision(Convert.ToInt32(row.ultimoPago), row.fecha_pago_comision);

                    dgvComisiones.Rows.Add(row.id_venta, row.nombre, row.ultimoPago, row.predio, row.manzana, row.lote, row.fecha_pago_comision);
                }
            }
            catch (NullReferenceException)
            {
                
            }
        }

        public void cargarDgv()
        {
            foreach (var row in comision._list)
            {
                status = validarStatusComision(Convert.ToInt32( row.ultimoPago), row.fecha_pago_comision);

                dgvComisiones.Rows.Add(row.id_venta, row.nombre, row.ultimoPago, row.predio, row.manzana, row.lote, row.fecha_pago_comision);
            }
        }

        public string validarStatusComision(int _ultimoPago, string _fechaPagoComision)
        {
            if (_ultimoPago >= 3)
            {
                return "ACTIVO";
            }
            if(_fechaPagoComision != "")
            {
                return "PAGADO";
            }

            return "SIN GENERAR";
        }

        private void tsBtnPagarComision_Click(object sender, EventArgs e)
        {
            int id_venta = Convert.ToInt32(dgvComisiones.CurrentRow.Cells[0].ToString());

            int index = _compraVeta._list.IndexOf(_compraVeta._list.Find(x => x.id_venta == id_venta));

            _compraVeta._list[index].fecha_pago_comision = DateTime.Now.ToString(_compraVeta.DATE_CAST);

            _compraVeta._list[index].update($"WHERE id_venta = {id_venta}");

            if (Mensaje.result_bool)
            {
                Mensaje.responseMessage("","",true);
            }
        }
    }
}
